
const express = require('express');
const router = require('./app/routes');
const app = express();
const port = 8080;
const expressLayouts = require('express-ejs-layouts');

app.use(express.static(`${dirname}/public`));

app.use(expressLayouts);

app.use('/', router);

app.listen(port, () => {
    console.log(`app started at port: ${port}`)
});
