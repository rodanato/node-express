
var express = require('express');
var router = express.Router();
var mainController = require('./controllers/main.controller');

module.exports = router;

router.get('/', function (req, res) {
    mainController.showHome;
});

router.get('/about', function (req, res) {
    mainController.showAbout;
});