
module.exports = {

  showHome : (req, res) => {
    res.render('pages/home');
    res.send('Hello world');
  },

  showAbout : (req, res) => {
    res.render('pages/about');
    res.send('About page');
  }
};