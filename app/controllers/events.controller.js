
module.exports = {

  showEvents: (req, res) => {

    const events = [
      { name: 'pedro', sport: 'basket'},
      { name: 'jose', sport: 'swimming'}
    ]
  
    res.render('pages/events', {events: events});
  }
};